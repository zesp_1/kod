#add a "quit" or "q" command to stop the code from running
x = ""
while True:
    x = input("Enter a number to count to: ")
    if x == "quit" or x == "q":
        break
    x = int(x)
    y = 1

    while True:
        print(y)
        y = y + 1
        if y > x:
            break

