#poprawiony skrypt okreslajacy jakiego rodzaju jest ACL na postawie jej numeru
#dodana konwersja aclNum ze string na int
aclNum = int(input("What is the IPv4 ACL number? "))

#poprawiona gorna granica zakresu(ze 199 na 99) w którym
#zalicza się ACL do list standardowych
if aclNum >= 1 and aclNum <= 99:
    print("This is a standard IPv4 ACL.")
elif aclNum >= 100 and aclNum <= 199:
    print("This is an extended IPv4 ACL.")
#zamiana elif na else jako ostatniej alternatywy dla funkcji if
else:
    print("This is not a standard or extended IPv4 ACL.")
